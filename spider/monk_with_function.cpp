#include <iostream>
#include <queue>
using namespace std;

//total number of spiders and spiders selected
int spiderCount, spiderSelected;

//queue to store power and position of spider
queue<pair<int, int>> powerQueue, tempQueue;

void findMaxPower()
{
	for (int spiderIndex = 0; spiderIndex<spiderSelected; spiderIndex++)
	{
		int limiter = 0;
		pair<int, int>maxPower = make_pair(-1, 0);
		while((!powerQueue.empty()) && (limiter < spiderSelected))
		{
			tempQueue.push(powerQueue.front());
			pair<int, int>tempPower = powerQueue.front();
			if(maxPower.first < tempPower.first)
				maxPower = tempPower;
			powerQueue.pop();
			limiter++;
		}
		cout<<maxPower.second +1 <<" ";
		pushToQueue(maxPower);
	}
}

void pushToQueue(pair<int, int> &maxPower)
{
	while(!tempQueue.empty())
	{
		pair<int, int>temp = tempQueue.front();
		if(temp != maxPower)
		{
			if(temp.first>0)
				temp.first - = 1;
			powerQueue.push(temp);
		}
		tempQueue.pop();
	}
	
}

int main()
{
	cin>> spiderCount;
	cin>>spiderSelected;
	for(int spiderIndex = 0; spiderIndex < spiderCount; spiderIndex++)
	{
		int spiderPower;
		cin>>spiderPower;
		powerQueue.push(make_pair(spiderPower, spiderIndex));
	}
	findMaxPower();
	return 0;
}