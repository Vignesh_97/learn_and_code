#include<iostream>
#include<queue>
using namespace std;
int main ()
{
	int limiter = 0,spiderCount,spiderSelected;
	queue<int> powerQueue;
	
	cin>>spiderCount;
	cin>>spiderSelected;
	int powerArray[spiderCount];
	for( int i = 0;i < spiderCount;i++)
	{
		cin>>powerArray[i];
		powerQueue.push(i);
	}
	while(limiter<spiderSelected)
	{
		int maxPower = -1, maxIndex, arrayIndex[spiderSelected], size = powerQueue.size();
		for(int i = 0;i < min(spiderSelected,size);i++)
		{
			arrayIndex[i]= powerQueue.front();
			powerQueue.pop();
			if (powerArray[arrayIndex[i]]>maxPower)
			{
				maxPower = powerArray[arrayIndex[i]];
				maxIndex = arrayIndex[i];
			}
		}
		cout<<maxIndex+1<<" ";
		for(int i = 0;i < min(spiderSelected,size);i++)
		{
			if (arrayIndex[i] == maxIndex)
				continue;
			else{
				if (powerArray[arrayIndex[i]])
					powerArray[arrayIndex[i]]--;
				powerQueue.push(arrayIndex[i]);
			}
		}
		limiter++;
	}
	return 0;
}