export class Employee {
    id: number;
    name: string;
    location: string;
    age: number;
}