import * as fs from "fs";
import * as read from "readline-sync";
import {Customer} from "./customer";
import {Employee} from "./employee";

export class Database {

    save(tableName): void {
        var details = getDataFromUser(tableName);
        storeToDatabase(details, tableName);
    }
    find(tableName, id) : void {
        fs.readFile(tableName + ".txt", function (err, data) {
        if (err) {
            throw new Error("Unable to read the file\n");
        } else {
            getRequiredData(id, data)
        }
    })

}

function getDataFromUser(tableName) {
    var data;
    if (tableName === "Customer") {
        data = new Customer();
    } else {
        data = new Employee();
    }
    data.id = read.question("Enter the id:");
    data.name = read.question("Enter the name:");
    data.location = read.question("Enter the location:");
    data.age = read.question("Enter the age:");
    return data;
};

function getRequiredData(id, data) {
    var rowIndex, record, dataRetrieved, flag = false;
    var dbRecords = data.toString().split("\n");
    for(rowIndex = 0; rowIndex < dbRecords.length; rowIndex++) {
        record = JSON.parse(dbRecords[rowIndex]);
        if (record.id === id) {
            dataRetrieved = record;
            flag = true;
            break;
        }
    }
    if (flag) {
        console.log(dataRetrieved);
    } else {
        console.log("Requested user id not found");
    }
}

function storeToDatabase(details, tableName) {
    details = JSON.stringify(details);
    fs.appendFile(tableName + ".txt", details + "\n", function (err) {
        if (err) {
            throw new Error("failed to store the data");
        } else {
            console.log(" Data is successfully stored");
        }
    });

}