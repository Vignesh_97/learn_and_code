import * as read from "readline-sync";
import {Database} from "./databaseOperations";

function main() {
    var dbOperation;
    dbOperation = read.question("Enter the type of operation to perform in database\n1)Store\n2)Retrieve");
    switch(dbOperation) {
        case 1 : storeDataToDB();
                   break;
        case 2 : retrieveDataFromDB();
                   break;
        default : console.log("Invalid input");
    }
}

function storeDataToDB() {
    var dataObj = new Database();
    var tableName = read.question("Enter a category to store the data: \n 1)Customer \n 2)Employee\n");
    dataObj.save(tableName);
}

function retrieveDataFromDB() {
    var id;
    var dataObj = new Database();
    var tableName = read.question("Enter a category to store the data: \n 1)Customer \n 2)Employee\n");
    id = read.question("Enter the id to obtain the data");
    dataObj.find(tableName, id);
}
main();

