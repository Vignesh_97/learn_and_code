
export class Customer{
    id: number;
    name: string;
    location: string;
    age: number;
}